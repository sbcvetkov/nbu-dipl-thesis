# nbu-dipl-thesis

This repo covers the containerized environment that is being used for the completion of my Diploma Thesis @ NBU.

In order to build the environment you have to meet the following requirements:

1. Install Docker on the machine
2. Set up the keys used to clone the repo on the machine
3. Clone the repo
4. Run `init.sh`

The `init.sh` script is a wrapper script used to generate self-signed certificates and also build the Docker environment. 